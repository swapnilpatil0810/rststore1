const products = [
	{
	  name: "Astrophysics for People in a Hurry",
	  image: "/images/81G4WoQ9t8L._AC_UF1000,1000_QL80_.jpg",
	  description:
		"Astrophysics for People in a Hurry is a book by Neil deGrasse Tyson that offers a concise introduction to the universe and its wonders, exploring concepts such as black holes, the big bang, and the nature of space and time.",
	  author: "Neil deGrasse Tyson",
	  category: "Astrophysics",
	  price: 852,
	  countInStock: 0,
	  rating: 4.6,
	  numReviews: 25,
	  releaseDate: "2 May 2017",
	},
	{
	  name: "A Brief History of Time",
	  image: "/images/71+qBHuR4yL._AC_UF1000,1000_QL80_.jpg",
	  description:
		"A Brief History of Time is a book by Stephen Hawking that explains complex theories of physics and cosmology, including the Big Bang theory, black holes, and the nature of time, in a way that is accessible to non-scientists.",
	  author: "Stephen Hawking",
	  category: "Astrophysics",
	  price: 260,
	  countInStock: 4,
	  rating: 4.8,
	  numReviews: 30,
	  releaseDate: "1 January 1998",
	},
	{
	  name: "Surely You're Joking, Mr. Feynman!",
	  image: "/images/71x3shLRNFL._AC_UF1000,1000_QL80_.jpg",
	  description:
		"Surely You're Joking, Mr. Feynman! is a collection of anecdotes and memoirs by Richard Feynman, a Nobel Prize-winning physicist. The book offers insights into Feynman's life, work, and his unique approach to science.",
	  author: "Richard Feynman",
	  category: "Physics",
	  price: 423,
	  countInStock: 6,
	  rating: 4.7,
	  numReviews: 15,
	  releaseDate: "24 July 1985",
	},
	{
	  name: "The Elegant Universe",
	  image: "/images/009928992X.01._SCLZZZZZZZ_SX500_.jpg",
	  description:
		"The Elegant Universe is a book by Brian Greene that explores the theories of relativity and quantum mechanics, as well as the concept of string theory, which seeks to unify these fundamental theories of physics.",
	  author: "Brian Greene",
	  category: "Physics",
	  price: 309,
	  countInStock: 3,
	  rating: 4.5,
	  numReviews: 20,
	  releaseDate: "16 November 2010",
	},
	{
	  name: "The Universe in a Nutshell",
	  image: "/images/61MEGY6i4aL._AC_UF1000,1000_QL80_.jpg",
	  description:
		"The Universe in a Nutshell is a book by Stephen Hawking that provides an overview of the latest developments in theoretical physics, including topics like black holes, the nature of time, and the search for a unified theory of everything.",
	  author: "Stephen Hawking",
	  category: "Physics",
	  price: 754,
	  countInStock: 5,
	  rating: 4.9,
	  numReviews: 18,
	  releaseDate: "5 November 2001",
	},
	{
	  name: "Relativity: The Special and General Theory",
	  image: "/images/41b9QByGizL.jpeg",
	  description:
		"Relativity: The Special and General Theory is a book by Albert Einstein that presents the theory of relativity in a comprehensible manner. Einstein explains the fundamental concepts and implications of his groundbreaking theory.",
	  author: "Albert Einstein",
	  category: "Physics",
	  price: 139,
	  countInStock: 8,
	  rating: 4.4,
	  numReviews: 12,
	  releaseDate: "1 December 2010",
	},
	{
	  name: "Storm in a Teacup: The Physics of Everyday Life",
	  image: "/images/35187175.jpeg",
	  description:
		"Storm in a Teacup: The Physics of Everyday Life is a book by Helen Czerski that explores the fascinating physics behind everyday phenomena. From the physics of soap bubbles to the science of popcorn popping, the book provides an entertaining and informative look at the world around us.",
	  author: "Helen Czerski",
	  category: "Physics",
	  price: 406,
	  countInStock: 9,
	  rating: 4.6,
	  numReviews: 22,
	  releaseDate: "1 June 2017",
	},
	{
	  name: "Dark Matter/Dark Energy (Hot Science): The Hidden 95% of the Universe",
	  image: "/images/71MzMSx33JL.jpeg",
	  description:
		"Dark Matter/Dark Energy (Hot Science): The Hidden 95% of the Universe is a book by Brian Clegg that delves into the mysteries of dark matter and dark energy. The book explores the current understanding and ongoing research in these areas, which are crucial for understanding the structure and evolution of the universe.",
	  author: "Brian Clegg",
	  category: "Physics",
	  price: 486,
	  countInStock: 6,
	  rating: 4.7,
	  numReviews: 17,
	  releaseDate: "8 August 2019",
	},
	{
	  name: "Reality Is Not What It Seems: The Journey to Quantum Gravity",
	  image: "/images/815VfpQtOdL._AC_UF1000,1000_QL80_.jpg",
	  description:
		"Reality Is Not What It Seems: The Journey to Quantum Gravity is a book by Carlo Rovelli that explores the quest for a theory that unifies general relativity and quantum mechanics. Rovelli takes readers on a journey through the history of physics and introduces the concepts and theories that shape our understanding of the universe.",
	  author: "Carlo Rovelli",
	  category: "Physics",
	  price: 1215,
	  countInStock: 7,
	  rating: 4.8,
	  numReviews: 18,
	  releaseDate: "3 July 2017",
	},
	{
	  name: "Starry Messenger: Cosmic Perspectives on Civilisation",
	  image: "/images/51geh1j1BrL.jpeg",
	  description:
		"Starry Messenger: Cosmic Perspectives on Civilisation is a book by Neil deGrasse Tyson that explores the connections between the universe and human civilization. Tyson discusses the role of astronomy in shaping our understanding of the cosmos and its impact on society and culture.",
	  author: "Neil deGrasse Tyson",
	  category: "Astronomy",
	  price: 1459,
	  countInStock: 5,
	  rating: 4.9,
	  numReviews: 16,
	  releaseDate: "25 December 2022",
	},
	{
	  name: "Something Deeply Hidden: Quantum Worlds and the Emergence of Spacetime",
	  image: "/images/71QrLejG-vL._AC_UF1000,1000_QL80_.jpg",
	  description:
		"Something Deeply Hidden: Quantum Worlds and the Emergence of Spacetime is a book by Sean Carroll that explores the foundations of quantum mechanics and the nature of reality. Carroll examines the many-worlds interpretation of quantum mechanics and its implications for our understanding of the universe.",
	  author: "Sean Carroll",
	  category: "Physics",
	  price: 934,
	  countInStock: 4,
	  rating: 4.5,
	  numReviews: 15,
	  releaseDate: "31 March 2021",
	},
	{
	  name: "Parallel Worlds: A Journey Through Creation, Higher Dimensions, and the Future of the Cosmos",
	  image: "/images/91xjg8Bp3bL._AC_UF1000,1000_QL80_.jpg",
	  description:
		"Parallel Worlds: A Journey Through Creation, Higher Dimensions, and the Future of the Cosmos is a book by Michio Kaku that explores the concept of parallel universes and the possibility of multiple dimensions. Kaku discusses the latest developments in physics and cosmology and their implications for our understanding of the universe.",
	  author: "Michio Kaku",
	  category: "Physics",
	  price: 1286,
	  countInStock: 3,
	  rating: 2.7,
	  numReviews: 12,
	  releaseDate: "14 February 2006",
	},
	{
	  name: "Black Holes and Baby Universes and Other Essays",
	  image: "/images/816EUgXpySL._AC_UF1000,1000_QL80_.jpg",
	  description:
		"Black Holes and Baby Universes and Other Essays is a collection of essays by Stephen Hawking that covers a wide range of topics in physics and cosmology. Hawking shares his insights and reflections on black holes, the nature of time, the origin of the universe, and other fascinating subjects.",
	  author: "Stephen Hawking",
	  category: "Physics",
	  price: 381,
	  countInStock: 8,
	  rating: 4.6,
	  numReviews: 14,
	  releaseDate: "8 September 1994",
	},
	{
	  name: "A Briefer History of Time",
	  image: "/images/81+gViMS3XL._AC_UF1000,1000_QL80_.jpg",
	  description:
		'A Briefer History of Time is a simplified and updated version of Stephen Hawking\'s best-selling book "A Brief History of Time." In this book, Hawking presents the key concepts of cosmology and the history of the universe in a more accessible and concise manner.',
	  author: "Stephen Hawking",
	  category: "Physics",
	  price: 232,
	  countInStock: 10,
	  rating: 4.5,
	  numReviews: 20,
	  releaseDate: "11 September 2008",
	},
  
	{
	  name: "The Theory of Everything",
	  image: "/images/449573.jpeg",
	  description:
		"The Theory of Everything is a book by Stephen Hawking that presents an overview of the fundamental theories of physics and cosmology, including the search for a unified theory that explains the laws of the universe.",
	  author: "Stephen Hawking",
	  category: "Physics",
	  price: 173,
	  countInStock: 5,
	  rating: 4.8,
	  numReviews: 18,
	  releaseDate: "25 September 2006",
	},
	{
	  name: "Chaos",
	  image: "/images/91N3RcpdOGL._AC_UF1000,1000_QL80_.jpg",
	  description:
		"Chaos is a book by James Gleick that explores the field of chaos theory and its implications across various disciplines, including physics, mathematics, biology, and social sciences. The book delves into the concept of unpredictability and the hidden patterns in seemingly random phenomena.",
	  author: "James Gleick",
	  category: "Physics",
	  price: 437,
	  countInStock: 6,
	  rating: 4.7,
	  numReviews: 15,
	  releaseDate: "24 February 1997",
	},
	{
	  name: "The Physics Book: Big Ideas Simply Explained",
	  image: "/images/61e8ixMixML.jpeg",
	  description:
		"The Physics Book: Big Ideas Simply Explained is a comprehensive guide that covers the key concepts and breakthroughs in physics. It presents complex theories in a simple and accessible manner, making it suitable for readers of all levels of understanding.",
	  author: "DK",
	  category: "Physics",
	  price: 905,
	  countInStock: 4,
	  rating: 3.6,
	  numReviews: 25,
	  releaseDate: "1 June 2020",
	},
	{
	  name: "Einstein for Everyone",
	  image: "/images/9788184950694_0_1610184559.jpeg",
	  description:
		"Einstein for Everyone is a book by Robert L. Piccioni that introduces the life and work of Albert Einstein, including his theories of relativity and contributions to physics. The book aims to make Einstein's ideas accessible to a general audience.",
	  author: "Robert L. Piccioni",
	  category: "Physics",
	  price: 261,
	  countInStock: 5,
	  rating: 4.5,
	  numReviews: 18,
	  releaseDate: "8 July 2010",
	},
	{
	  name: "A Short History of Nearly Everything",
	  image: "/images/a-really-short-history-of-nearly-everything-1.jpeg",
	  description:
		"A Short History of Nearly Everything is a book by Bill Bryson that explores various scientific disciplines, including physics, chemistry, geology, and biology. Bryson presents complex scientific concepts in a humorous and engaging way, making it accessible to a wide audience.",
	  author: "Bill Bryson",
	  category: "Science",
	  price: 437,
	  countInStock: 3,
	  rating: 3.7,
	  numReviews: 22,
	  releaseDate: "19 September 2016",
	},
	{
	  name: "Black Holes: The Reith Lectures",
	  image: "/images/51w6EZdGCFL.jpeg",
	  description:
		"Black Holes: The Reith Lectures is a book by Stephen Hawking that features a series of lectures delivered by Hawking, exploring the concept of black holes and their significance in astrophysics. The book offers insights into the nature and behavior of these enigmatic cosmic phenomena.",
	  author: "Stephen Hawking",
	  category: "Physics",
	  price: 130,
	  countInStock: 6,
	  rating: 4.6,
	  numReviews: 20,
	  releaseDate: "11 July 2016",
	},
  ];
  
  export default products;